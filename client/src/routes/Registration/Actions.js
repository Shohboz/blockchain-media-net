
const Reflux = require('reflux');

const Actions = Reflux.createActions([
  'register',
]);

module.exports = Actions;
