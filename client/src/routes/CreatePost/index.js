
const React = require('react');
const Reflux = require('reflux');

const Store = require('./Store');
const Actions = require('./Actions');

const MetaMenu = require('./MetaMenu');
const PostEdit = require('./PostEdit');
const PostView = require('../../components/PostView');

class Page extends Reflux.Component {

  constructor(props) {
    super(props);
    this.store = Store;
  }

  previewTabClick(e) {
    e.preventDefault();
    Actions.previewClick();
  }

  editTabClick(e) {
    e.preventDefault();
    Actions.editorClick();
  }

  createButtonClick(e) {
    e.preventDefault();
    Actions.createClick();
  }

  render() {
    let editLinkClass = 'nav-link';
    let previewLinkClass = 'nav-link';

    let editTabClass = 'tab-pane fade';
    let previewTabClass = 'tab-pane fade';

    if( this.state.preview ) {
      previewLinkClass += ' active';
      previewTabClass += ' show active';
    }

    if( this.state.editor ) {
      editLinkClass += ' active';
      editTabClass += ' show active';
    }

    let errors = [];
    let preloader = [];
    for(let index in this.state.errors)
      errors.push(<div className="alert alert-warning" key={'error' + index}><strong>ERROR!</strong> {this.state.errors[ index ]}</div>);

    if(this.state.formInprogress)
      preloader.push(<div key="preloader" className="preloader"><img src="/img/ajax-loader.gif" /></div>);

    return (<div>
      <h1 className="text-center">Create thread</h1>
      <br/>

      <div className="row">
        <div className="col-md-9">

          <ul className="nav nav-tabs" id="myTab" role="tablist">
            <li className="nav-item">
              <a className={editLinkClass} href="#postEditTab" onClick={this.editTabClick}>Post edit</a>
            </li>
            <li className="nav-item">
              <a className={previewLinkClass} href="#postPreviewTab" onClick={this.previewTabClick}>Preview</a>
            </li>
          </ul>
          <div className="tab-content" id="myTabContent">
            <div className={editTabClass} id="home" role="tabpanel" aria-labelledby="home-tab">
              <br/>
              <PostEdit />
            </div>
            <div className={previewTabClass} id="profile" role="tabpanel" aria-labelledby="profile-tab">
              <br/>
              <PostView
                postTitle={this.state.postTitle}
                postBlocks={this.state.postBlocks || '[]'}
              />
            </div>
          </div>

        </div>
        <div className="col-md-3">
          {errors}
          <div className="form-group">
            <button className="btn btn-primary btn-block" onClick={this.createButtonClick}>Create</button>
          </div>
          <MetaMenu />
        </div>
      </div>
      {preloader}
    </div>)
  }
}

module.exports = Page;
