
const Reflux = require('reflux');

const Actions = Reflux.createActions([
  'accountUpdate',
  'signOut',
  'ddClick',
  'ddBackDropClick',
  'linkClick'
]);

module.exports = Actions;
