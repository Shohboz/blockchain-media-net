
const util = require('util');
const eosjs =  require('eosjs');
const random = require('random');
const fetch = require('node-fetch');
const JsSignatureProvider = require('eosjs/dist/eosjs-jssig').JsSignatureProvider;

class BlockChainConnector {

  constructor(config) {
    this.config = config;
  }

  EOSPost( data, user) {
    let signatureProvider = new JsSignatureProvider([ user.privateKey ]);
    let rpc = new eosjs.JsonRpc(this.config.apiUrl[ random.int(0, this.config.apiUrl.length-1) ], { fetch });
    let api = new eosjs.Api({ rpc, signatureProvider, textDecoder: new util.TextDecoder(), textEncoder: new util.TextEncoder() });

    return api.transact({
      actions: [
        {
          account: this.config.contract,
          name: 'post',
          data: {
            user: user.name,
            threadData: data
          },
          authorization: [
            {
              actor: user.name,
              permission: 'active',
            }
          ]
        }
      ]
    },
    {
      blocksBehind: 3,
      expireSeconds: 30,
    });
  }

  EOSComment( postTRXID, parentTRXID, data, user) {    
    let signatureProvider = new JsSignatureProvider([ user.privateKey ]);
    let rpc = new eosjs.JsonRpc(this.config.apiUrl[ random.int(0, this.config.apiUrl.length-1) ], { fetch });
    let api = new eosjs.Api({ rpc, signatureProvider, textDecoder: new util.TextDecoder(), textEncoder: new util.TextEncoder() });

    return api.transact({
      actions: [
        { // name user, std::string postTRXID, std::string parentTRXID, std::string commentData
          account: this.config.contract,
          name: 'comment',
          data: {
            user: user.name,
            postTRXID: postTRXID,
            parentTRXID: parentTRXID,
            commentData: data
          },
          authorization: [
            {
              actor: user.name,
              permission: 'active',
            }
          ]
        }
      ]
    },
    {
      blocksBehind: 3,
      expireSeconds: 30,
    });
  }

  EOSUserInfo(userName) {
    let rpc = new eosjs.JsonRpc(this.config.apiUrl[ random.int(0, this.config.apiUrl.length-1) ], { fetch });
    return rpc.get_account(userName);
  }

  post( data, user) {
    if( this.config.type == 'EOS' )
      return this.EOSPost( data, user);
  }

  comment( postTRXID, parentTRXID, data, user) {
    if( this.config.type == 'EOS' )
      return this.EOSComment( postTRXID, parentTRXID, data, user);
  }

  userInfo(userName) {
    if( this.config.type == 'EOS' )
      return this.EOSUserInfo(userName);
  }

}

module.exports = BlockChainConnector;
