
const Joi = require('joi');

const schema = Joi.object({
  id: Joi.number().integer().example(1),
  blockchain_user: Joi.string().example('medeostest11'),
  blockchain_trx_id: Joi.string().example('bdc8ee8eb44e421577b5f0d222fe9b5ca831810fd0549af795f82bed83d627a2'),
  blockchain_block_id: Joi.number().integer(31797219),
  post_blockchain_block_id: Joi.number().integer(31797219),
  post_blockchain_trx_id: Joi.string().example('bdc8ee8eb44e421577b5f0d222fe9b5ca831810fd0549af795f82bed83d627a2'),
  parent_blockchain_block_id: Joi.number().integer().allow([null]).example(31797219),
  parent_blockchain_trx_id: Joi.string().allow(['', null]).example('bdc8ee8eb44e421577b5f0d222fe9b5ca831810fd0549af795f82bed83d627a2'),
  body: Joi.string().example('Comment body'),
  file: Joi.string().allow(['', null]).example('data:image/jpeg;base64,/...'),
  file_name: Joi.string().allow(['', null]).example('test_comment_image.jpg'),
  file_type: Joi.string().allow(['', null]).example('image'),
  updatedAt: Joi.date().example('2019-02-16T15:38:48.243Z'),
  createdAt: Joi.date().example('2019-02-16T15:38:48.243Z')
});

module.exports = schema;
