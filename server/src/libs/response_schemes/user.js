
const Joi = require('joi');

const schema = Joi.object({
  id: Joi.number().integer().example(1),
  email: Joi.string().example('pupkin@gmail.com'),
  eos_name: Joi.string().allow(['', null]).example('pupkin.eosio'),
  eos_public: Joi.string().allow(['', null]).example('EOS8DorxGMFqPWd8DuCFMv9MtiuCQsj2WkmXfFPpZY15xFmwxLg5s'),
  eos_private: Joi.string().allow(['', null]).example('EOS8DorxGMFqPWd8DuCFMv9MtiuCQsj2WkmXfFPpZY15xFmwxLg5s'),
  updatedAt: Joi.date().example('2019-02-16T15:38:48.243Z'),
  createdAt: Joi.date().example('2019-02-16T15:38:48.243Z')
});

module.exports = schema;
