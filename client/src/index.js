'use strict'

const React = require('react');
const ReactDOM = require('react-dom');
const Router = require('react-micro-router');
const Route = Router.Route;

const Account = require('./libs/Account');
const appConfig = require('../config');

const Wrapper = require('./components/Wrapper');
const Home = require('./routes/Home');
const Search = require('./routes/Search');
const Registration = require('./routes/Registration');
const SignIn = require('./routes/SignIn');
const Profile = require('./routes/Profile');
const ProfileContent = require('./routes/Profile/Content');
const CreatePost = require('./routes/CreatePost');
const Post = require('./routes/Post');
const Sync = require('./routes/Sync');


let app = (<div>
  <Route path="/" exact>
    <Wrapper page={Home} />
  </Route>

  <Route path="/search" exact>
    <Wrapper page={Search} />
  </Route>

  <Route path="/post/([0-9:a-f]+)" >
    <Wrapper page={Post} />
  </Route>

  <Route path="/registration" exact>
    <Wrapper page={Registration} />
  </Route>
  
  <Route path="/sync" exact>
    <Wrapper page={Sync} />
  </Route>

  <Route path="/create" exact>
    <Wrapper page={CreatePost} />
  </Route>

  <Route path="/sign_in" exact>
    <Wrapper page={SignIn} />
  </Route>

  <Route path="/profile" exact>
    <Wrapper page={Profile} />
  </Route>

  <Route path="/profile/content" exact>
    <Wrapper page={ProfileContent} />
  </Route>

</div>);

window.account = new Account(appConfig);
window.account.init()
.then( (authorized) => {
  ReactDOM.render(
    app,
    document.getElementById('root')
  );
});
