
const Joi = require('joi');

const metaSchema = require('../../../libs/response_schemes/meta');
const buildResult = require('../../../libs/controllersHelpers').buildResult;
const queryHelper = require('../../../libs/queryHelper');

const commentSchema = require('../../../libs/response_schemes/comment');

async function response(request) {

  const comments = request.getModel(request.server.config.db.database, 'comments');

  let paramsParts = request.params.post_id.split(':');

  let blockID = parseInt(paramsParts[ 0 ]);
  let trxID = paramsParts[ 1 ];

  let queryParams = queryHelper.parseQueryParams(request.query);
  queryParams.where.post_blockchain_block_id = blockID;
  queryParams.where.post_blockchain_trx_id = trxID;

  let resultPosts = await comments.findAll(queryParams);

  let result = [];
  for(let item of resultPosts)
    result.push(item.get({ plain: true }));

  return buildResult(result, 1, result.length, 0, null);
}

const responseSchema = Joi.object({
  meta: metaSchema,
  data: Joi.array().items(commentSchema)
});

module.exports = {
  method: 'GET',
  path: '/api/post/{post_id}/comments',
  options: {
    handler: response,
    description: 'Get post by blockID:trx_ID',
    notes: 'Get post by blockID:trx_ID',
    tags: [ 'api' ],
    auth: false,
    validate: {
      params: {
        post_id: Joi.string().description('blockID:trx_ID').example('28610416:be0a0734a0b785398243e5710f5db98a072dd4bf00e6ecbfbc7cfcd91a743014')
      },
      query: {
        __order: Joi.string().allow(null).default('').description('Query order').example('createdAt desc').default('createdAt desc'),
        __count: Joi.number().integer().allow(null).min(1).max(100).default(20).description('Necessary rows count').example(10),
        __offset: Joi.number().integer().allow(null).min(0).default(0).description('Offset').example(0)
      }
    },
    response: { schema: responseSchema }
  }
};
