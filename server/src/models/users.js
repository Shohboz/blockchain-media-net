'use strict';

const Bcrypt = require('bcryptjs');
const bcryptRounds = 10;

module.exports = (sequelize, DataTypes) => {
  const users = sequelize.define('users', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    email: {
      type: DataTypes.STRING
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false
    },
    eos_name: {
      type: DataTypes.STRING
    },
    eos_private: {
      type: DataTypes.STRING
    },
    eos_public: {
      type: DataTypes.STRING
    },
  });

  users.hashPassword = function(unencrypted) {
    return Bcrypt.hashSync(unencrypted, bcryptRounds);
  };

  users.verifyPassword = function(unencrypted, encrypted) {
    return Bcrypt.compareSync(unencrypted, encrypted);
  };

  users.dummyData = [
    {
      id: 1,
      email: 'pupkin@gmail.com',
      password: users.hashPassword('12345'),
      eos_name: 'medeostest11',
      eos_private: '5JGwstdCwyiEh7pCfPGcRdqFcR8ZW1b2cpTjwE8TWRF15Xz3BFb',
      eos_public: 'EOS8DorxGMFqPWd8DuCFMv9MtiuCQsj2WkmXfFPpZY15xFmwxLg5s'
    }
  ];

  return users;
};
