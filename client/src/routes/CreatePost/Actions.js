
const Reflux = require('reflux');

const Actions = Reflux.createActions([
  'dataLoad',
  'createClick',
  'previewClick',
  'editorClick',
  'addBlock',
  'deleteBlock',
  'textBodyChange',
  'titleChange',
  'imagePositionUpdate',
  'imageUpdate',
  'imageTitleUpdate',
  'fieldUpdate'
]);

module.exports = Actions;
