'use strict';

const statuses = {
  INIT: { id: 0, name: 'INIT' },
  INPROGRESS: { id: 1, name: 'INPROGRESS' },
  DONE: { id: 2, name: 'DONE' },
  ERROR: { id: 3, name: 'ERROR' }
};

const dummyData = [];

module.exports = (sequelize, DataTypes) => {

  const Blocks = sequelize.define('blocks', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    blockchain_id: {
      type: DataTypes.INTEGER,
      unique: true
    },
    status: {
      type: DataTypes.INTEGER,
    },
    error_message: {
      type: DataTypes.STRING,
      allow: null
    },
    thread_id: {
      type: DataTypes.STRING,
      allow: null
    }
  }, {
    indexes: [
      {
        name: 'blocks_blockchain_id_index',
        method: 'BTREE',
        fields: [ 'blockchain_id' ]
      },
    ]
  });

  Blocks.statuses = statuses;
  Blocks.statusesById = {};

  for(let index in statuses)
    Blocks.statusesById[ statuses[ index ].id ] = statuses[ index ];

  Blocks.dummyData = dummyData;

  return Blocks;
};
