
const Reflux = require('reflux');

const Actions = Reflux.createActions([
  'loadPosts',
]);

module.exports = Actions;
