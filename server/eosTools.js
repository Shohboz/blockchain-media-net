'use strict';

const eosTools = require('./src/libs/eosTools');

async function main() {
  if( process.argv.length < 3 ) {
    console.log('Not enough arguments');
    console.log('Usage: node eosTools.js getBlock|getInfo [block_num]');
    console.log('Example: node eosTools.js getBlock 2106228');
    console.log('Example: node eosTools.js getInfo');
    process.exit(1);
  }

  // --- getBlock
  if( process.argv[ 2 ] == 'getBlock' ) {
    if( !process.argv[ 3 ] ) {
      console.log('Not enough argument: block number');
      process.exit(0);
    }

    let blockNum = parseInt(process.argv[ 3 ]);
    //let result = await rpc.get_block(blockNum);
    let result = await eosTools.getBlock(blockNum);

    console.log( JSON.stringify(result, null, ' ') );
    process.exit(0);
  }

  // --- getInfo
  if( process.argv[ 2 ] == 'getInfo' ) {
    console.log( JSON.stringify(await eosTools.getInfo(), null, ' ') );
    process.exit(0);
  }

  console.log('Unknown argument', process.argv[ 2 ]);
  process.exit(1);
}

main();
