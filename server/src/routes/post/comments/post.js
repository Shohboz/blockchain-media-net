
const Joi = require('joi');
const Boom = require('boom');

const metaSchema = require('../../../libs/response_schemes/meta');
const buildResult = require('../../../libs/controllersHelpers').buildResult;

const commentSchema = require('../../../libs/response_schemes/comment');

const BlockChainConnector = require('../../../libs/BlockChainConnector');

async function response(request) {

  let connector = new BlockChainConnector(request.server.config.blockchain);
  const comments = request.getModel(request.server.config.db.database, 'comments');

  let BCUser = {
    name: request.auth.artifacts.user.eos_name,
    privateKey: request.auth.artifacts.user.eos_private,
    publicKey: request.auth.artifacts.user.eos_public,
  };

  let idParts = request.params.post_id.split(':');
  let post_blockchain_trx_id = idParts[ 1 ];
  let post_blockchain_block_id = parseInt(idParts[ 0 ]);
  
  let requestComment = Object.assign({}, request.payload);
  requestComment.post_blockchain_block_id = post_blockchain_block_id;
  requestComment.post_blockchain_trx_id = post_blockchain_trx_id;

  let BCResult;

  try { // postTRXID, parentTRXID, data, user
    BCResult = await connector.comment( requestComment.post_blockchain_trx_id, requestComment.parent_blockchain_trx_id || '' , JSON.stringify(requestComment), BCUser);
  } catch(err) {
    console.log('* ERR:', err);
    throw Boom.unauthorized(err);
  }

  let newComment = {
    blockchain_user: BCUser.name,
    blockchain_trx_id: BCResult.transaction_id,
    blockchain_block_id: BCResult.processed.block_num,
    post_blockchain_block_id: post_blockchain_block_id,
    post_blockchain_trx_id: post_blockchain_trx_id,
    parent_blockchain_block_id: request.payload.parent_blockchain_block_id || null,
    parent_blockchain_trx_id: request.payload.parent_blockchain_trx_id || null,
    body: request.payload.body,
    file: request.payload.file,
    file_name: request.payload.file_name,
    file_type: request.payload.file_type,
    createdAt: BCResult.processed.block_time,
    updatedAt: BCResult.processed.block_time
  };

  let newDBComment = await comments.create(newComment);

  return buildResult([ newDBComment.dataValues ], 1, 1, 0, null);
}

const responseSchema = Joi.object({
  meta: metaSchema,
  data: Joi.array().items(commentSchema)
});

module.exports = {
  method: 'POST',
  path: '/api/post/{post_id}/comments',
  options: {
    handler: response,
    description: 'Create comment',
    notes: 'Create comment',
    tags: [ 'api' ],
    auth: 'token',
    validate: {
      headers: Joi.object({
        authorization: Joi.string().required().description('Authorization token').example('Bearer d585649bad7421a9b6c84531bad992cc')
      }).options({ allowUnknown: true }),
      params: {
        post_id: Joi.string().description('Post block_id:trx_id').example('31797219:bdc8ee8eb44e421577b5f0d222fe9b5ca831810fd0549af795f82bed83d627a2')
      },
      payload: {
        body: Joi.string().description('Comment body text').example('Hello guys!'),
        file: Joi.string().allow(['', null]).description('Attached file body in base64').example('data:image/jpeg;base64,/...'),
        file_name: Joi.string().allow(['', null]).description('Attached file name or file description').example('My_image.jpg'),
        file_type: Joi.string().allow(['', null]).description('File type').example('image'),
        parent_blockchain_block_id: Joi.number().integer().allow(['', null]).description('Parent comment reference').example(33472627),
        parent_blockchain_trx_id: Joi.string().allow(['', null]).description('Parent comment reference').example('1b8ad507c069f70815248f91965b3eb28f248a25b6e81991dba437bc49f5ba7e'),
        parent_user: Joi.string().allow(['', null]).description('Parent comment reference').example('pupkin123')
      }
    },
    response: { schema: responseSchema }
  }
};
