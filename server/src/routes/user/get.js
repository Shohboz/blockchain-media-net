
const Joi = require('joi');

const buildResult = require('../../libs/controllersHelpers').buildResult;
const metaSchema = require('../../libs/response_schemes/meta');
const userSchema = require('../../libs/response_schemes/user');

async function response(request) {

  let exportUser = {
    id: request.auth.artifacts.user.id,
    email: request.auth.artifacts.user.email,
    eos_name: request.auth.artifacts.user.eos_name,
    eos_public: request.auth.artifacts.user.eos_public,
    eos_private: request.auth.artifacts.user.eos_private,
    createdAt: request.auth.artifacts.user.createdAt,
    updatedAt: request.auth.artifacts.user.updatedAt,
  };

  return buildResult([ exportUser ], 1, 1, 0, null);
}

const responseSchema = Joi.object({
  meta: metaSchema,
  data: Joi.array().items(userSchema)
});

module.exports = {
  method: 'GET',
  path: '/api/user',
  options: {
    handler: response,
    description: 'Get current user',
    notes: 'Get current user by auth token',
    tags: [ 'api' ],
    auth: 'token',
    validate: {
      headers: Joi.object({
        authorization: Joi.string().required().description('Authorization token').example('Bearer d585649bad7421a9b6c84531bad992cc')
      }).options({ allowUnknown: true })
    },
    response: { schema: responseSchema }
  }
};
