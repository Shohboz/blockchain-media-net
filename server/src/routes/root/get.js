
async function response() {
  return {
    result: 'ok',
    message: 'Hello World!'
  };
}

module.exports = {
  method: 'GET',
  path: '/api',
  options: {
    handler: response,
    tags: ['api'], // Necessary tag for swagger
    validate: {
    }
  }
};