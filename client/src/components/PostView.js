
const React = require('react');

const postTools = require('../libs/postTools');

class TextBlock extends React.Component {
  render() {
    let html = postTools.preparePost(this.props.body);

    return(<p className="text-block" dangerouslySetInnerHTML={{ __html: html }}></p>)
  }
}

class ImageBlock extends React.Component {

  render() {
    let alignClass = `block-image-${this.props.align}`;

    if( this.props.title ) {
      return(<div className={alignClass}>
        <img src={this.props.body} />
        <div className="block-image-title"><span>{this.props.title}</span></div>
      </div>)
    } else {
      return(<div className={alignClass}>
        <img src={this.props.body} />
      </div>)
    }
  }
}

class PostView extends React.Component {

  constructor(props) {
    super(props);
  }

  render() {
    let blocks = [];

    for(let index in this.props.postBlocks) {
      if( this.props.postBlocks[ index ].type == 'text' )
        blocks.push(<TextBlock
          key={index}
          blockIndex={index}
          body={this.props.postBlocks[ index ].body}
        />);

      if( this.props.postBlocks[ index ].type == 'image' )
        blocks.push(<ImageBlock
          key={index}
          blockIndex={index}
          body={this.props.postBlocks[ index ].body}
          align={this.props.postBlocks[ index ].align}
          title={this.props.postBlocks[ index ].title}
        />);
    }

    return(<div>
      <h1>{this.props.postTitle}</h1>
      {blocks}
    </div>)
  }

}

module.exports = PostView;
