'use strict';

const voteTypes = {
  like: { id: 1, name: 'like' },
  dislike: { id: 0, name: 'dislike' }
};

module.exports = (sequelize, DataTypes) => {
  const post_votes = sequelize.define('post_votes', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    blockchain_user: {
      type: DataTypes.STRING
    },
    blockchain_trx_id: {
      type: DataTypes.STRING
    },
    blockchain_block_id: {
      type: DataTypes.STRING
    },
    post_blockchain_block_id: {
      type: DataTypes.INTEGER
    },
    post_blockchain_trx_id: {
      type: DataTypes.STRING
    },
    type: {
      type: DataTypes.INTEGER
    },
  });

  post_votes.voteTypes = voteTypes;

  post_votes.dummyData = [];

  return post_votes;
};
