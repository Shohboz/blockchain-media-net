
const React = require('react');
const Reflux = require('reflux');

const Store = require('./Store');
const Actions = require('./Actions');

const PostItemBlock = require('../../components/PostItemBlock');

class Page extends Reflux.Component {

  constructor(props) {
    super(props);
    this.store = Store;
  }

  componentDidMount() {
    Actions.loadChainInfo();
  }

  render() {
    
    if( this.state.syncInprogress === true ) {
      return (<div>
        <h1>Sync info</h1>
        <div key="preloader" className="preloader"><img src="/img/ajax-loader.gif" /></div>
      </div>)
    }
    
    let items = [];
    for(let index in this.state.chainInfo)
      items.push(<div key={index}><label>{index}:</label> <span>{this.state.chainInfo[ index ]}</span></div>);

    return (<div>
      <h1>Sync info</h1>
      {items}
    </div>)
  }
}

module.exports = Page;
