
const Reflux = require('reflux');

const Actions = Reflux.createActions([
  'signIn',
]);

module.exports = Actions;
